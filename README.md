# debos-recipes

A set of [debos](https://github.com/go-debos/debos) recipes for building a
debian-based image for Librem 5 device.  
This image ships an installer based on calamares, supporting LUKS, deploying the image on the eMMC 
**CAUTION: This set of debos recipes is not meant for end users, 
it is there to help assess how (nearly)vanilla Debian system performs on a Librem 5 device!!**


The default user is `user` with password `123456`.
The default `root` password is `root`.

## Build

The build process is automated using a CI pipeline (see .gitlab-ci.yml). 
The installer image becomes available as a pipeline artefact. 

Debos uses fakemachine and requires access to /dev/kvm. 
Either you have a dedicated gitlab runner with /dev/kvm access or 
build on your own computer using the commands defined in .gitlab-ci.yml.  

## Image content

The debos recipes generate a rootfs packed as a squashfs and stored inside the installer image.
The installer is the final image that the Librem 5 has to boot from.
The installer rely on calamares to deploy the rootfs onto the device (on the eMMC).

## Notes on the installer - how it works
Calamares is started in frame buffer mode via a systemd service (see calamares-debian.service in overlay-installer)
Following that, calamares reads the /etc/calamares/settings.conf and further scripts are called accordingly.
Calamares is configured to use an OSK based on the Plasma virtual keyboard.

## Notes on the overlay/scripts content - workarounds meant to go away
The overlay used for the rootfs should disappear: the files in there should either be fixed upstreamed (MM) or moved to mobile-tweaks
The overlay used for the installer should go in a dedicated calamares-settings mobile package.
Similarly, most of the content of scripts/setup-{kernel,uboot,installer} should be removed after mainline support is available

The installer uses the mobile extensions from calamares-extensions which isn't yet packaged in Debian (it is pulled from mobian repo).
Hence the calamares version used must matched the version against which calamares-extensions was built in mobian (bullseye) from shared library to load properly. 
Device specific installer script parts should ideally be split off the main scripts so those can be kept generic, and invoked from a dedicated directory using run-parts, for example.

## Flashing
Host pre-requisites: 
1. uuu installed
2. Micro sd card plugged with at least 4GB

Run the flashing script such as: `sudo ./flashing/download-image-and-flash.sh`
This script will:
1. download the installer image from the last successful CI run.
2. extract throm that installer the artefacts required to boot the L5 from the micro-sd.
3. prompt you to add an sdcard on the host and provide the script with the path to the sdcard on which the image shall be written.
4. prompt you to set the L5 in flashing mode (see [0]) and to plug the micro-sd with the installer in it
5. run uuu installer_l5.lst - effectively booting the L5 from the installer image on the micro-sd

# License

This software is licensed under the terms of the GNU General Public License,
version 3.

# Credits
These recipes are heavily inspired from the Mobian recipes

[0] https://developer.puri.sm/Librem5/Development_Environment/Phone/Troubleshooting/Reflashing_the_Phone.html
