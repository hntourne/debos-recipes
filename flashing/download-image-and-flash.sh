#!/bin/bash

cd `dirname $0`
usage() { echo "Usage: $0 -c <path/sdcard>" 1>&2; exit 1; }

while getopts "c:" option; do
    case "${option}" in
        c)
            SDCARD=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
if [ -z "${SDCARD}" ] ; then
    usage
fi

# cleanup artefacts leftovers
rm imx8*dtb
rm u-boot-librem5.bin
rm initramfs-purism-librem5.gz
rm kernel-librem5
rm debian-librem5.img artefacts.zip

wget -O artefacts.zip https://source.nilux.be/images/debos-librem5/-/jobs/artifacts/master/download?job=installer
unzip artefacts.zip

partx -a -v debian-librem5.img
mount /dev/loop0p2 /mnt/
mount /dev/loop0p1 /mnt/boot
cp /mnt/usr/lib/u-boot/librem5/u-boot.imx u-boot.imx
cp /mnt/boot/initrd.img-* initramfs-purism-librem5.gz
cp /mnt/boot/vmlinuz-* kernel-librem5
find /mnt/ -type f -name "imx8mq-librem5*.dtb" -exec cp -t . {} \;
umount /mnt/boot/
umount /mnt
losetup -d /dev/loop0

read -p "Press ENTER when ready to write the installer image to your sdcard (at ${SDCARD})" ready
dd if=debian-librem5.img of="${SDCARD}"
sync

read -p "Press ENTER when the sdcard is in the phone, and the phone is in flashing mode" ready
uuu installer_l5.lst

exit 0


