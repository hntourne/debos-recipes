#!/bin/sh

USERNAME=$1
[ "$USERNAME" ] || exit 1

# Disable power key handling to avoid accidental shutdown mid-install
mkdir -p /etc/systemd/logind.conf.d
cat > /etc/systemd/logind.conf.d/10-ignore-power-key.conf << EOF
[Login]
HandlePowerKey=ignore
EOF

# Setup ondevice installer to start on boot
systemctl enable calamaresfb.service
systemctl enable serial-getty@ttyS0
chmod 0644 /usr/lib/systemd/system/calamares-debian.service

# Rename user so installer can change it's password
if [ -f /etc/calamares/modules/mobile.conf ] && [ "$USERNAME" != "user" ]; then
    sed -i "s/username: \"user\"/username: \"$USERNAME\"/" /etc/calamares/modules/mobile.conf
fi

wget -O /tmp/calamares.deb http://snapshot.debian.org/archive/debian/20211112T154313Z/pool/main/c/calamares/calamares_3.2.44.3-1%2Bb2_arm64.deb
wget -O /tmp/calamares-ext-data.deb https://repo.mobian-project.org/pool/main/c/calamares-extensions/calamares-extensions-data_1.1.2-1_all.deb
wget -O /tmp/calamares-ext.deb https://repo.mobian-project.org/pool/main/c/calamares-extensions/calamares-extensions_1.1.2-1_arm64.deb
apt install -y /tmp/calamares-ext.deb /tmp/calamares-ext-data.deb /tmp/calamares.deb
