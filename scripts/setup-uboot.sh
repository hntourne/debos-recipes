#!/bin/sh

wget -O /tmp/uboot.zip https://source.puri.sm/Librem5/uboot-imx/-/jobs/artifacts/librem5/download?job=build:cross:debian-buster
unzip /tmp/uboot.zip -d /tmp
dpkg -i /tmp/debian/output/u-boot-librem5_*.deb

# this is needed when this script is ran non-chrooted in the debos env
test -d /etc/default || mkdir /etc/default
touch /etc/default/u-boot

if ! grep -q "dtb-" /etc/default/u-boot; then
cat > /etc/default/u-boot << EOF
## /etc/default/u-boot - configuration file for u-boot-update(8)

U_BOOT_UPDATE="true"
U_BOOT_ALTERNATIVES="default"
U_BOOT_MENU_LABEL="Debian GNU/Linux"
U_BOOT_PARAMETERS="console=ttymxc3,115200 consoleblank=0 loglevel=7 rw splash plymouth.ignore-serial-consoles vt.global_cursor_default=0"
U_BOOT_TIMEOUT="10"
U_BOOT_FDT_DIR="/boot/dtbs/"
EOF
fi

# Create boot menu file for u-boot
which u-boot-update && u-boot-update
