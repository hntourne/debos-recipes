#!/bin/sh

wget -O /tmp/kernel.zip https://source.puri.sm/Librem5/linux-next/-/jobs/artifacts/pureos/byzantium/download?job=build:deb
wget -O /tmp/flash-kernel.deb https://repo.pureos.net/pureos/pool/main/f/flash-kernel/flash-kernel_3.102.0pureos4_arm64.deb
unzip /tmp/kernel.zip -d /tmp/

mkdir /etc/flash-kernel && echo "Purism Librem 5r4" > /etc/flash-kernel/machine

dpkg -i /tmp/flash-kernel.deb
dpkg -i /tmp/debian/output/linux-image-5*_arm64.deb

# this is needed when this script is ran non-chrooted in the debos env
test -d /etc/modprobe.d/ || mkdir /etc/modprobe.d/
# upstream firmware needed for this driver is available in the package firmware-misc-nonfree
echo "blacklist rsi_91x" >> /etc/modprobe.d/wifi.conf
echo "blacklist rsi_sdio" >> /etc/modprobe.d/wifi.conf
echo "options redpine_91x dev_oper_mode=5 rsi_zone_enabled=1 antenna_diversity=1" >> /etc/modprobe.d/wifi.conf
update-initramfs -u -k all
